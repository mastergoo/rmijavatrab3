/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * 
 * @author aluno
 */
public class IpServer implements DataBasesIp {

	private String iphost;
	private int idxServer = 0;

	public static void main(String[] args) {
		try {
			IpServer server = new IpServer();
			server.iphost = args[0];
			DataBasesIp stubIp = (DataBasesIp) UnicastRemoteObject.exportObject(server, 0);
			Registry reg = LocateRegistry.getRegistry(server.iphost);
			reg.rebind("//" + server.iphost + "/ip", stubIp);
			System.err.println("Servidor carregado");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public String getIp() throws RemoteException {
		int tmp = idxServer;
		ArrayList<String> servers = getServers();
		String[] ips = new String[servers.size()];
		idxServer = (idxServer + 1) % ips.length;
		return ips[tmp];
	}

	@Override
	public java.sql.Connection getConexao() throws RemoteException {
		try {
			return DriverManager.getConnection("jdbc:postgresql://localhost:5432/ipserver", "root", "");
		} catch (SQLException e) {
			throw new RuntimeException("Erro ao conectar");
		}
	}

	@Override
	public ArrayList<String> getServers() throws RemoteException {
		String query = "SELECT * FROM ips";
		try {
			PreparedStatement ps = getConexao().prepareStatement(query);
			ResultSet querySet = ps.executeQuery();
			ArrayList<String> ips = new ArrayList<String>();
			while (querySet.next() == true) {
				ips.add(querySet.getString(1).trim());
			}
			ps.close();
			return ips;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface DataBaseServers extends Remote {
	public java.sql.Connection getConexao() throws RemoteException;

	public boolean addServer(String ip) throws RemoteException;

	public boolean serverExists(String ip) throws RemoteException;
}

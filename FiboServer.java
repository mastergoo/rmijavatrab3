/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 
 * @author aluno
 */
public class FiboServer implements Fibonacci, DataBaseServers {

	private String host;

	public static void main(String[] args) {
		try {
			FiboServer server = new FiboServer();
			server.host = args[0];
			Fibonacci stubfibo = (Fibonacci) UnicastRemoteObject.exportObject(server, 0);
			server.addServer(server.host);
			Registry reg = LocateRegistry.getRegistry(server.host);
			reg.rebind("//" + server.host + "/fibo", stubfibo);
			System.err.println("Servidor carregado");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public long getN(int n) throws RemoteException {
		long a = 0, b = 1;
		while (n-- > 0) {
			long next = a + b;
			a = b;
			b = next;
		}
		return b;
	}

	@Override
	public java.sql.Connection getConexao() throws RemoteException {
		try {
			return DriverManager.getConnection("jdbc:postgresql://localhost:5432/ipserver", "root", "");
		} catch (SQLException e) {
			throw new RuntimeException("Erro ao conectar");
		}
	}

	@Override
	public boolean addServer(String ip) throws RemoteException {
		if (!serverExists(ip)) {
			String query = "INSERT INTO ips VALUES(?)";
			try {
				PreparedStatement ps = getConexao().prepareStatement(query);
				ps.setString(1, ip);
				ps.execute();
				ps.close();
				return true;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	@Override
	public boolean serverExists(String ip) throws RemoteException {
		String query = "SELECT * FROM ips";
		try {
			PreparedStatement ps = getConexao().prepareStatement(query);
			ResultSet querySet = ps.executeQuery();
			while (querySet.next() == true) {
				if (ip.equals(querySet.getString(1).trim())) {
					return true;
				}
			}
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
}
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface DataBasesIp extends Remote {
	public java.sql.Connection getConexao() throws RemoteException;

	public ArrayList<String> getServers() throws RemoteException;

	public String getIp() throws RemoteException;
}
